#!/bin/bash

xsltproc index.xslt menu.xml > ./generated/index.html

xsltproc Schema.xslt ../XMLSource/Norway.xml > ./Generated/Norway.html
echo Norway: $?
xsltproc Schema.xslt ../XMLSource/Czechia.xml > ./Generated/Czechia.html
echo Czechia: $?
xsltproc Schema.xslt ../XMLSource/Japan.xml > ./Generated/Japan.html
echo Japan: $?
xsltproc Schema.xslt ../XMLSource/Australia.xml > ./Generated/Australia.html
echo Australia: $?
