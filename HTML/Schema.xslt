<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" method="html"/>
    <xsl:template match="/country">
        <html>
            <head>
                <title><xsl:value-of select="name"/></title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="States.css"></link>
            </head>
            <body>
                <h1>
                    <xsl:value-of select="@name"/>
                </h1>
                <div class="flag">
                    <img>
                        <xsl:attribute name="src">
                            <xsl:text>img/</xsl:text>
                            <xsl:value-of select="@name"/>
                            <xsl:text>.svg</xsl:text>
                        </xsl:attribute>
                    </img>
                </div>
                <h2>Obsah</h2>
                <ul class="content">
                    <xsl:for-each select="categories/category">
                        <li>
                            <a>
                                <xsl:attribute name="href">
                                    <xsl:text>#</xsl:text>
                                    <xsl:value-of select="@title"/>
                                </xsl:attribute>
                                <xsl:value-of select="@title"/>
                            </a>
                        </li>
                    </xsl:for-each>
                </ul>
                <div class="category">
                    <xsl:apply-templates select="categories/category"/>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="categories/category">
        <h2>
            <xsl:attribute name="id">
                <xsl:value-of select="@title"/>
            </xsl:attribute>
            <xsl:value-of select="@title"/>
        </h2>
        <xsl:apply-templates select="sections/section"/>
    </xsl:template>
    <xsl:template match="sections/section">
        <h3>
            <xsl:value-of select="@title"/>
        </h3>
        <xsl:apply-templates select="OptionalKeyPairs/value"/>
    </xsl:template>
    <xsl:template match="OptionalKeyPairs/value">
        <div>
            <xsl:choose>
                <xsl:when test="@key != ''">
                    <span>
                        <xsl:value-of select="@key"/>:
                    </span>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            <xsl:value-of select="."/>
        </div>
    </xsl:template>
</xsl:stylesheet>
