<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Jakub Volák - XML Semestral project</title>
                <link rel="stylesheet" href="Default.css"></link>
            </head>
            <body>
                <div class="mainWrapper">
                    <h1>States</h1>
                    <xsl:apply-templates select="countries/country"/>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="countries/country">
        <p>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="."/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </a>
        </p>
    </xsl:template>
</xsl:stylesheet>
