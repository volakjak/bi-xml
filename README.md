BI-XML Semestrální projekt - Jakub Volák / volakjak

Vybrané země:
- Norsko
- Česká republika
- Japonsko
- Austrálie

Potřebný SW:
- sudo apt install libxml2-utils
- sudo apt install xsltproc
- sudo apt install fop

Popis:
- Práce je rozdělená do složek podle toho, co generují. Ve složce XMLSource se nachází zdrojové xml soubory jednotlivých zemí, které jsem vytvořil ručně.
- Ve složkách DTD a RELAXNG se nachází validační schéma a skript, který validaci provede. Ve výpisu je pak počet chyb jednotlivých souborů. Skripty jsou vytvořeny ručně pro dané soubory, nejsou automatizovány.
- Ve složce HTML se nachází schéma pro hlavní stránku s navigací a také schéma pro stránky jednotlivých zemí. Ve složce je opět skript, který provede vygenerování html souborů do podadresáře. V podadresáři se nacházejí také styly a obrázky pro html výstup.
- Ve složce PDF se nachází schémata pro vytvoření jednoho PDF souboru, který obsahuje všechny vybrané státy. Je zde také skript, který provede vygenerování PDF souboru. V PDF se využívají obrázky ze složky HTML.