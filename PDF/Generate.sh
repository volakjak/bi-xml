#!/bin/bash

xsltproc index.xslt ../HTML/menu.xml > ./generated/index.xml

xsltproc Schema.xslt ../XMLSource/Norway.xml > ./Generated/Norway.xml
echo Norway: $?
xsltproc Schema.xslt ../XMLSource/Czechia.xml > ./Generated/Czechia.xml
echo Czechia: $?
xsltproc Schema.xslt ../XMLSource/Japan.xml > ./Generated/Japan.xml
echo Japan: $?
xsltproc Schema.xslt ../XMLSource/Australia.xml > ./Generated/Australia.xml
echo Australia: $?

cat ./Generated/Norway.xml >> ./Generated/All.xml
cat ./Generated/Czechia.xml >> ./Generated/All.xml
cat ./Generated/Japan.xml >> ./Generated/All.xml
cat ./Generated/Australia.xml >> ./Generated/All.xml

sed -i -e '/INSERT_STATES/ r ./Generated/All.xml' ./Generated/index.xml
sed -i -e '/INSERT_STATES/d' ./Generated/index.xml

echo WORKING...
fop ./Generated/index.xml States.pdf &> /dev/null
echo PDF: $?

rm ./Generated/All.xml
