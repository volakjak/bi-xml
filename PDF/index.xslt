<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="main" margin-bottom="0.7cm" margin-top="0.7cm">
                    <fo:region-body margin-top="1cm" margin-bottom="0.5cm" />
                    <fo:region-before extent="1cm" />
                    <fo:region-after extent="0.3cm" font-size="10pt" />
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="main" force-page-count="no-force">
                <fo:flow flow-name="xsl-region-body" text-align="center">
                    <fo:block margin="1cm 1cm" font-size="32pt" font-weight="400">
                        States
                    </fo:block>
                    <xsl:apply-templates select="countries/country"/>
                </fo:flow>
            </fo:page-sequence>
            INSERT_STATES
        </fo:root>
    </xsl:template>
    <xsl:template match="countries/country">
        <fo:block margin="0.5cm 0" font-size="16pt" font-weight="300">
            <fo:basic-link>
                <xsl:attribute name="internal-destination">
                    <xsl:value-of select="."/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </fo:basic-link>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>
