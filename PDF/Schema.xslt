<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xsl:template match="/country">
        <fo:page-sequence force-page-count="no-force" margin-left="1cm" margin-right="1cm" master-reference="main">
            <fo:static-content color="black" flow-name="xsl-region-after">
                <fo:block text-align="center">
                    <fo:page-number/>
                </fo:block>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
                <xsl:attribute name="id">
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
                <fo:block margin="0.5cm 0" font-size="32pt" font-weight="400" text-align="center">
                    <xsl:value-of select="@name"/>
                </fo:block>
                <fo:block text-align="center">
                    <fo:external-graphic border="1px solid #000000" content-width="scale-to-fit" max-width="200pt" scaling="uniform">
                        <xsl:attribute name="src">
                            <xsl:text>../../HTML/Generated/img/</xsl:text>
                            <xsl:value-of select="@name"/>
                            <xsl:text>.svg</xsl:text>
                        </xsl:attribute>
                    </fo:external-graphic>
                </fo:block>
                <xsl:apply-templates select="categories/category"/>
            </fo:flow>
        </fo:page-sequence>
    </xsl:template>
    <xsl:template match="categories/category">
        <fo:block>
            <fo:block margin-top="0.5cm" margin-bottom="0.3cm" font-size="22pt" font-weight="400">
                <xsl:value-of select="@title"/>
            </fo:block>
            <xsl:apply-templates select="sections/section"/>
        </fo:block>
    </xsl:template>
    <xsl:template match="sections/section">
        <fo:block margin-top="0.2cm" margin-bottom="0.1cm" margin-left="0.2cm" font-size="13pt" font-weight="400">
            <xsl:value-of select="@title"/>
        </fo:block>
        <xsl:apply-templates select="OptionalKeyPairs/value"/>
    </xsl:template>
    <xsl:template match="OptionalKeyPairs/value">
        <fo:block margin-left="0.7cm">
            <xsl:choose>
                <xsl:when test="@key != ''">
                    <fo:inline margin-right="1cm" font-size="11pt" font-weight="600">
                        <xsl:value-of select="@key"/>:
                    </fo:inline>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            <fo:inline margin-left="0.5cm" font-size="11pt" font-weight="300">
                <xsl:value-of select="."/>
            </fo:inline>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>
