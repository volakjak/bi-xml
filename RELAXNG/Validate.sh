#!/bin/bash
# sudo apt install libxml2-utils

xmllint --noout --relaxng Schema.xml "../XMLSource/Czechia.xml"
echo Czechia: $?
xmllint --noout --relaxng Schema.xml "../XMLSource/Norway.xml"
echo Norway: $?
xmllint --noout --relaxng Schema.xml "../XMLSource/Japan.xml"
echo Japan: $?
xmllint --noout --relaxng Schema.xml "../XMLSource/Australia.xml"
echo Australia: $?
