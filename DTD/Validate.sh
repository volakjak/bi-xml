#!/bin/bash
# sudo apt install libxml2-utils

xmllint --noout --dtdvalid Schema.dtd "../XMLSource/Czechia.xml"
echo Czechia: $?
xmllint --noout --dtdvalid Schema.dtd "../XMLSource/Norway.xml"
echo Norway: $?
xmllint --noout --dtdvalid Schema.dtd "../XMLSource/Japan.xml"
echo Japan: $?
xmllint --noout --dtdvalid Schema.dtd "../XMLSource/Australia.xml"
echo Australia: $?
